L-System Fractal Renderer
=========================


How To build
------------

1. Have `Qt5`
2. `git clone` this repository and `cd` into it
3. `qmake && make`
4. Executable is inside `bin`
