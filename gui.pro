SOURCES += src/main.cpp
SOURCES += src/gl.cpp
SOURCES += src/window.cpp
SOURCES += src/lsystem.cpp
SOURCES += src/delegate.cpp
SOURCES += src/fractal.cpp
SOURCES += src/view.cpp
SOURCES += src/generator.cpp
SOURCES += src/graphicsobject.cpp

HEADERS += include/gl.h
HEADERS += include/window.h
HEADERS += include/lsystem.h
HEADERS += include/delegate.h
HEADERS += include/vertexdata.h
HEADERS += include/fractal.h
HEADERS += include/vertex.h
HEADERS += include/view.h
HEADERS += include/version.h
HEADERS += include/generator.h
HEADERS += include/graphicsobject.h

INCLUDEPATH += include
INCLUDEPATH += ui

FORMS += ui/main.ui

RESOURCES += gui.qrc

QMAKE_CXXFLAGS += -Wextra -pedantic -g

CONFIG += -Wall -Wextra -pedantic c++14 qt debug
QT = core gui widgets

OBJECTS_DIR = build
UI_DIR = ui
MOC_DIR = build
DESTDIR = bin

TARGET = gui
