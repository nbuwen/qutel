colors = """
rgb(153,0,0), rgb(166,88,0), rgb(195,230,57), rgb(0,204,255), rgb(0,0,242), rgb(230,0,214), rgb(229,115,115), rgb(153,117,77), rgb(117,153,77), rgb(83,149,166), rgb(0,0,217), rgb(166,83,149), rgb(255,115,64), rgb(255,191,64), rgb(0,191,51), rgb(41,108,166), rgb(0,0,191), rgb(191,48,105), rgb(255,179,128), rgb(153,143,0), rgb(121,242,218), rgb(64,140,255), rgb(113,89,179), rgb(255,0,68)
""".strip().split(', ')

colors = [color[4:-1] for color in colors]
colors = [[float(i) / 255 for i in color.split(',')] for color in colors]

for color in colors:
    color = ', '.join(map(str, color))
    print(f"vec3({color}),")

print(len(colors))
