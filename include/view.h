#pragma once


#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QRectF>
#include <vertex.h>


class View
{
public:
    void initialize();
    void resize(int width, int height);
    void synchronize(QOpenGLShaderProgram&) const;
    bool handleKey(int);

    void lookAt(Vertex);
private:
    QMatrix4x4 projection;

    QVector3D position;
    QVector3D forward;
    QVector3D left;
    QVector3D up;

    float width;
    float height;

    void updateProjection();
};
