#pragma once


#include <QOpenGLWidget>
#include <QOpenGLDebugLogger>

#include <fractal.h>


class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    GLWidget(QWidget* parent=nullptr);

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void setViewCenter(Vertex);

    void saveImage();
    void toggleAA();
signals:
    void finishInitialize();
    void preparePaint(View&);
    void finishPaint();
protected:
    void keyPressEvent(QKeyEvent*) override;
private:
    View view;

    void log(const QOpenGLDebugMessage&);

    QOpenGLDebugLogger* logger;
    void initializeLogger();
};
