#pragma once

#include <QOpenGLFunctions_3_3_Core>

namespace opengl
{

using Functions = QOpenGLFunctions_3_3_Core;
struct {
	int Major = 3;
	int Minor = 3;
} Version;

}
