#pragma once


#include <qopengl.h>

struct __attribute__ ((packed)) Vertex
{
    float x;
    float y;
    float z;
    float color;
};

static_assert(sizeof(GLuint) == 4, "o");
