#pragma once

#include <array>
#include <cinttypes>

#include <QVector3D>
#include <QQuaternion>

#include <vertexdata.h>

enum class Action
{
    Nothing,
    Left,
    Right,
    Forward,
    Push,
    Pop,
    PitchDown,
    PitchUp,
    RollLeft,
    RollRight,
    TurnAround,
    Skip,
    NextColor,
    StartFace,
    EndFace,
};


class Generator
{
    std::array<Action, 256> actions;

    VertexData lines;
    VertexData faces;
    VertexData* current = &lines;

    float angle;

    struct State
    {
        QVector3D position;
        QVector3D heading;
        QVector3D left;
        QVector3D up;
        uint16_t colorIndex;
    };

    State state;

    std::vector<State> stack;

public:
    void clear();
    void addAction(char symbol, Action action);
    void setAngle(float angle);
    void generate(const std::string& state);
    void moveForward();
    void rotate(float);
    void pitch(float);
    void roll(float);
    void pushState();
    void popState();
    void turnAround();
    void skip();
    void nextColor();
    void startFace();
    void endFace();

    void addVertex();

    VertexData&& moveLines();
    VertexData&& moveFaces();
};
