#pragma once


#include <vector>
#include <algorithm>

#include <qopengl.h>

#include <vertex.h>

struct VertexData
{
    std::vector<Vertex> vertices;
    std::vector<GLint> starts;
    std::vector<GLsizei> lengths;
    GLsizei currentStripLength;

    Vertex center;

    size_t strips() const
    {
        return starts.size();
    }

    size_t vertexCount() const
    {
        return vertices.size();
    }

    size_t byteSize() const
    {
        return vertices.size() * sizeof(Vertex);
    }

    void clear()
    {
        currentStripLength = 0;
        vertices.clear();
        starts = {0};
        center = {0, 0, 0, 0};
    }

    void addOrigin()
    {
        ++currentStripLength;
        vertices.push_back({0, 0, 0, 0});
    }

    void addVertex(float x, float y, float z, float color)
    {
        vertices.push_back({x, y, z, color});
        ++currentStripLength;
    }

    void insertCut()
    {
        starts.push_back(vertexCount());
        lengths.push_back(currentStripLength);
        currentStripLength = 0;
    }

    void finish()
    {
        lengths.push_back(currentStripLength);
        currentStripLength = 0;

        calculateCenter();
    }

    void calculateCenter()
    {
        float minX = std::numeric_limits<float>::max();
        float maxX = std::numeric_limits<float>::min();
        float minY = std::numeric_limits<float>::max();
        float maxY = std::numeric_limits<float>::min();

        for (const auto& vertex : vertices)
        {
            if (vertex.x < minX) minX = vertex.x;
            if (vertex.x > maxX) maxX = vertex.x;
            if (vertex.y < minY) minY = vertex.y;
            if (vertex.y > maxY) maxY = vertex.y;
        }

        center.x = (minX + maxX) / 2;
        center.y = (minY + maxY) / 2;
    }
};
