#pragma once


#include <array>
#include <utility>
#include <vector>


using Symbol = char;
using String = std::string;
using Rules = std::array<String, 256>;


struct Rule
{
    Symbol predecessor;
    String successor;
};


class LSystem
{
    Rules rules;
    Symbol axiom;

    mutable std::vector<String> cache;

    void updateCache() const;
    void iterate() const;
public:
    LSystem();

    const String& iteration(size_t) const;
    void clear();
    void setAxiom(Symbol);
    void setRules(const std::vector<Rule>&);
    void addRule(const Rule&);
    void addRule(Symbol, String&&);
};
