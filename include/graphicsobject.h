#pragma once


#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

#include <vertexdata.h>

class GraphicsObject
{
    QOpenGLBuffer vertexBuffer;
    QOpenGLVertexArrayObject arrayObject;

    VertexData data;
public:
    void initialize();
    void update(VertexData);
    void paint(int mode);

    Vertex center() const;
};
