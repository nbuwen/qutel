#pragma once

#include <QMainWindow>

#include <ui_main.h>
#include <lsystem.h>
#include <generator.h>
#include <view.h>


struct SavedSystem
{
    QString name;
    QString axiom;
    float angle;

    std::vector<Rule> rules;
};

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT
public:
    MainWindow(LSystem&, Fractal&, Generator&);

    std::vector<Color> colorList() const;

private:
    LSystem& system;
    Fractal& fractal;
    Generator& generator;

    std::vector<SavedSystem> savedSystems;

    void updateSystem();
    void updatePreview();
private slots:
    void finishInitializeGL();
    void preparePaintGL(View&);
    void finishPaintGL();

    void on_update_clicked();

    void on_previewIteration_valueChanged(int);
    void on_axiom_editingFinished();
    void on_rules_cellChanged(int, int);
    void on_load_currentIndexChanged(int);
    void on_colors_cellDoubleClicked(int, int);

    void loadSystems();
};
