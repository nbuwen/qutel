#pragma once

#include <QOpenGLShaderProgram>

#include <view.h>
#include <generator.h>
#include <graphicsobject.h>


struct Color
{
    GLfloat r;
    GLfloat g;
    GLfloat b;
};

class Fractal
{
private:
    QOpenGLShaderProgram shaderProgram;

    GraphicsObject lineObject;
    GraphicsObject faceObject;
public:
    void initialize();
    void update(Generator&);
    void prepare(View&);
    void paint();
    void syncColors(const std::vector<Color>&);

    Vertex center() const;
};
