#include <generator.h>


void Generator::clear()
{
	actions.fill(Action::Nothing);
}

void Generator::addAction(char symbol, Action action)
{
	actions[symbol] = action;
}

void Generator::setAngle(float angle)
{
    this->angle = angle;
}

void Generator::moveForward()
{
    state.position += state.heading;
    addVertex();
}

void Generator::rotate(float angle)
{
    auto q = QQuaternion::fromAxisAndAngle(state.up, angle);
    state.heading = q.rotatedVector(state.heading);
    state.left = q.rotatedVector(state.left);
}

void Generator::pitch(float angle)
{
    auto q = QQuaternion::fromAxisAndAngle(state.left, angle);
    state.heading = q.rotatedVector(state.heading);
    state.up = q.rotatedVector(state.up);
}

void Generator::roll(float angle)
{
    auto q = QQuaternion::fromAxisAndAngle(state.heading, angle);
    state.left = q.rotatedVector(state.left);
    state.up = q.rotatedVector(state.up);
}

void Generator::pushState()
{
    stack.push_back(state);
}

void Generator::addVertex()
{
    current->addVertex(
        state.position.x(),
        state.position.y(),
        state.position.z(),
        state.colorIndex
    );
}

void Generator::popState()
{
    state = std::move(stack.back());
    stack.pop_back();

    current->insertCut();
    addVertex();
}

void Generator::turnAround()
{
    //auto q = QQuaternion::fromAxisAndAngle(state.up, 180);
    //state.heading = q.rotatedVector(state.heading);
    //state.left = q.rotatedVector(state.left);
    //state.up *= -1;
    state.heading *= -1;
    state.left *= -1;
    //state.up *= -1;
}

void Generator::skip()
{

    current->insertCut();
    state.position += state.heading;
    addVertex();
}

void Generator::nextColor()
{
    ++state.colorIndex;
}

void Generator::startFace()
{
    lines.insertCut();
    current = &faces;
    addVertex();
}

void Generator::endFace()
{
    faces.insertCut();
    current = &lines;
    addVertex();
}


void Generator::generate(const std::string& state)
{
    lines.clear();
    lines.addOrigin();
    faces.clear();

    this->state = {
        {0, 0, 0},
        {0, 1, 0},
        {1, 0, 0},
        {0, 0, 1},
        0
    };

    stack.clear();

    for (char symbol : state)
    {
        switch(actions[symbol])
        {
        case Action::Nothing:
            break;
        case Action::Forward:
            moveForward();
            break;
        case Action::Left:
            rotate(angle);
            break;
        case Action::PitchDown:
            pitch(-angle);
            break;
        case Action::PitchUp:
            pitch(angle);
            break;
        case Action::RollLeft:
            roll(-angle);
            break;
        case Action::RollRight:
            roll(angle);
            break;
        case Action::Right:
            rotate(-angle);
            break;
        case Action::TurnAround:
            turnAround();
            break;
        case Action::Push:
            pushState();
            break;
        case Action::Pop:
            popState();
            break;
        case Action::Skip:
            skip();
            break;
        case Action::NextColor:
            nextColor();
            break;
        case Action::StartFace:
            startFace();
            break;
        case Action::EndFace:
            endFace();
            break;
        }
    }
    lines.finish();
    faces.finish();
}


VertexData&& Generator::moveLines()
{
    return std::move(lines);
}

VertexData&& Generator::moveFaces()
{
    return std::move(faces);
}
