#include <graphicsobject.h>


#include <version.h>

void GraphicsObject::initialize()
{
    arrayObject.create();
    arrayObject.bind();

    vertexBuffer.create();
    vertexBuffer.bind();
    vertexBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    vertexBuffer.allocate(nullptr, 0);

    auto f = QOpenGLContext::currentContext()->versionFunctions<opengl::Functions>();

    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    f->glEnableVertexAttribArray(1);
    auto offset = reinterpret_cast<void*>(offsetof(Vertex, color));
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), offset);

    vertexBuffer.release();
    arrayObject.release();
}


void GraphicsObject::update(VertexData newData)
{
    data = std::move(newData);

    arrayObject.bind();
    vertexBuffer.bind();

    vertexBuffer.allocate(data.vertices.data(), data.byteSize());

    vertexBuffer.release();
    arrayObject.release();
}

void GraphicsObject::paint(int mode)
{
    arrayObject.bind();

    auto f = QOpenGLContext::currentContext()->versionFunctions<opengl::Functions>();
    f->glMultiDrawArrays(mode, data.starts.data(), data.lengths.data(), data.strips());

    arrayObject.release();
}

Vertex GraphicsObject::center() const
{
    return data.center;
}
