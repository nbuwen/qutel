#include <window.h>

#include <fstream>

#include <QColorDialog>

#include <delegate.h>



MainWindow::MainWindow(LSystem& system, Fractal& fractal, Generator& generator)
    : QMainWindow(), system(system), fractal(fractal), generator(generator)
{
    setupUi(this);


    auto delegate = new ComboBoxDelegate(renderActions);
    renderActions->setItemDelegateForColumn(1, delegate);

    connect(gl, &GLWidget::finishInitialize, this, &MainWindow::finishInitializeGL);
    connect(gl, &GLWidget::preparePaint, this, &MainWindow::preparePaintGL);
    connect(gl, &GLWidget::finishPaint, this, &MainWindow::finishPaintGL);

    loadSystems();
    updateSystem();
}

void MainWindow::finishInitializeGL()
{
    fractal.initialize();
    fractal.syncColors(colorList());
}

void MainWindow::preparePaintGL(View& view)
{
    fractal.prepare(view);
}

void MainWindow::finishPaintGL()
{
    fractal.paint();
}

void MainWindow::on_update_clicked()
{
    generator.clear();

    for (long row = 0; row < renderActions->rowCount(); ++row)
    {
        auto symbolItem = renderActions->item(row, 0);
        auto actionItem = renderActions->item(row, 1);

        if (symbolItem)
        {
            auto symbols = symbolItem->text().toStdString();
            auto action = actionItem->data(Qt::DisplayRole).toInt(nullptr);

            for (auto symbol : symbols)
            {
                if (symbol != ' ')
                {
                    generator.addAction(symbol, static_cast<Action>(action));
                }
            }
        }
    }

    generator.setAngle(angle->value());
    generator.generate(system.iteration(previewIteration->value()));

    fractal.update(generator);
    gl->setViewCenter(fractal.center());
}

void MainWindow::on_previewIteration_valueChanged(int)
{
    updateSystem();
}

void MainWindow::on_axiom_editingFinished()
{
    updateSystem();
}

void MainWindow::on_rules_cellChanged(int, int)
{
    updateSystem();
}

void MainWindow::updateSystem()
{
    if (axiom->text().isEmpty())
    {
        return;
    }

    system.clear();
    for (long row = 0; row < rules->rowCount(); ++row)
    {
        auto preItem = rules->item(row, 0);
        auto sucItem = rules->item(row, 1);

        if (preItem)
        {
            auto pre = preItem->text().toStdString();
            auto suc = sucItem? sucItem->text().toStdString() : "";
            system.addRule(pre[0], std::move(suc));
        }
    }

    system.setAxiom(axiom->text().toStdString()[0]);

    updatePreview();
}

void MainWindow::on_colors_cellDoubleClicked(int row, int)
{
    auto background = colors->item(row, 0)->background();
    auto newColor = QColorDialog::getColor(background.color(), this);
    background.setColor(newColor);
    colors->item(row, 0)->setBackground(background);

    fractal.syncColors(colorList());
}

std::vector<Color> MainWindow::colorList() const
{
    std::vector<Color> list;
    for (int row = 0; row < colors->rowCount(); ++row)
    {
        auto color = colors->item(row, 0)->background().color();
        list.push_back({
            float(color.redF()), float(color.greenF()), float(color.blueF())
        });
    }
    return list;
}

void MainWindow::updatePreview()
{
    auto n = previewIteration->value();
    const auto& it = system.iteration(n);

    if (it.size() > 2000)
    {
        preview->setText(QString::fromStdString(it.substr(0, 2000)));
    }
    else
    {
        preview->setText(QString::fromStdString(it));
    }
}


void MainWindow::loadSystems()
{
    std::ifstream fin;
    fin.exceptions(fin.exceptions() | std::ios::badbit);
    fin.open("systems.txt");

    std::string command;

    savedSystems.clear();
    load->clear();

    while (std::getline(fin, command))
    {
        if (command[0] == '#') continue;
        if(command.find_first_not_of(' ') == std::string::npos) continue;

        if (command.rfind("Name", 0) == 0) {
            savedSystems.push_back({
                QString::fromStdString(command.substr(5)),
                "",
                0,
                {}
            });
        } else if (command.rfind("Axiom", 0) == 0) {
            savedSystems.at(savedSystems.size() - 1).axiom += command.at(6);
        } else if (command.rfind("Angle", 0) == 0) {
            savedSystems.at(savedSystems.size() - 1).angle = std::stof(command.substr(6));
        } else if (command[0] != ' ' && command[1] == ' ') {
            savedSystems.at(savedSystems.size() - 1).rules.push_back({
                command[0], command.substr(2)
            });
        }
    }

    for (auto& s : savedSystems) {
        load->addItem(s.name);
    }
}

void MainWindow::on_load_currentIndexChanged(int index)
{
    const auto& currentSystem = savedSystems[index];

    angle->setValue(currentSystem.angle);
    axiom->setText(currentSystem.axiom);

    rules->setRowCount(0);
    rules->setRowCount(5);

    for (size_t row = 0; row < currentSystem.rules.size(); ++row)
    {
        auto& rule = currentSystem.rules[row];
        rules->insertRow(row);

        auto pre = new QTableWidgetItem(QString() + rule.predecessor);
        auto suc = new QTableWidgetItem(QString::fromStdString(rule.successor));
        rules->setItem(row, 0, pre);
        rules->setItem(row, 1, suc);
    }
}
