#include "lsystem.h"

#include <iostream>

LSystem::LSystem()
{
    clear();
}

void LSystem::setAxiom(Symbol symbol)
{
    axiom = symbol;
    updateCache();
}

void LSystem::setRules(const std::vector<Rule>& rawRules)
{
    rules.fill("");
    for (const Rule& rule : rawRules)
    {
        addRule(rule);
    }
    updateCache();
}


void LSystem::addRule(const Rule& rule)
{
    size_t index = static_cast<size_t>(rule.predecessor);
    rules[index] = rule.successor;
}


void LSystem::addRule(Symbol predecessor, String&& successor)
{
    size_t index = static_cast<size_t>(predecessor);
    rules[index] = successor;
}

void LSystem::updateCache() const
{
    cache.resize(1);
    cache[0] = axiom;
}

void LSystem::clear()
{
    for (size_t symbol = 0; symbol < rules.size(); ++symbol)
    {
        rules[symbol] = symbol;
    }
    axiom = '\0';
}


const String& LSystem::iteration(size_t n) const
{
    while (cache.size() <= n)
    {
        iterate();
    }

    return cache[n];
}

void LSystem::iterate() const
{
    const String& current = cache[cache.size() - 1];
    std::string next;

    for (Symbol symbol : current)
    {
        next += rules[symbol];
    }
    cache.emplace_back(std::move(next));
}
