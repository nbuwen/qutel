#include <view.h>


#include <QQuaternion>


void View::initialize()
{
    position = {0, 0, 1};
    forward = {0, 0, -1};
    left = {-1, 0, 0};
    up = {0, 1, 0};

    updateProjection();
}

void View::resize(int width, int height)
{
    this->width = width;
    this->height = height;
    updateProjection();
}

void View::synchronize(QOpenGLShaderProgram& shaderProgram) const
{
    shaderProgram.bind();
    shaderProgram.setUniformValue(shaderProgram.uniformLocation("projection"), projection);
}

bool View::handleKey(int key)
{
    switch (key)
    {
    case Qt::Key_Up:
        position += forward / 2;
        break;
    case Qt::Key_Down:
        position -= forward / 2;
        break;
    case Qt::Key_Left:
        position += left / 2;
        break;
    case Qt::Key_Right:
        position -= left / 2;
        break;
    case Qt::Key_Plus:
        position += up / 2;
        break;
    case Qt::Key_Minus:
        position -= up / 2;
        break;
    case Qt::Key_W: {
            auto q = QQuaternion::fromAxisAndAngle(left, 5);
            forward = q.rotatedVector(forward);
            up = q.rotatedVector(up);
        }
        break;
    case Qt::Key_S: {
            auto q = QQuaternion::fromAxisAndAngle(left, -5);
            forward = q.rotatedVector(forward);
            up = q.rotatedVector(up);
        }
        break;
    case Qt::Key_A: {
            auto q = QQuaternion::fromAxisAndAngle(up, 5);
            forward = q.rotatedVector(forward);
            left = q.rotatedVector(left);
        }
        break;
    case Qt::Key_D: {
            auto q = QQuaternion::fromAxisAndAngle(up, -5);
            forward = q.rotatedVector(forward);
            left = q.rotatedVector(left);
        }
        break;
    default:
        return false;
    }
    updateProjection();
    return true;
}

void View::updateProjection()
{
    projection.setToIdentity();

    projection.perspective(90, width / height, 0.1, 1000);
    projection.lookAt(position, position + forward, up);
}

void View::lookAt(Vertex center)
{
    position = QVector3D{center.x, center.y, 0} - forward;
    updateProjection();
}
