#include <QApplication>
#include <QSurfaceFormat>
#include <QOpenGLContext>

#include <window.h>
#include <fractal.h>
#include <lsystem.h>
#include <version.h>
#include <generator.h>


void setOpenGLFormat()
{
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setSamples(16);
    format.setVersion(opengl::Version.Major, opengl::Version.Minor);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setOption(QSurfaceFormat::DebugContext);

    QSurfaceFormat::setDefaultFormat(format);
}

int main(int argc, char *argv[])
{
    setOpenGLFormat();

    LSystem system;
    Fractal fractal;
    Generator generator;

    QApplication app(argc, argv);

    MainWindow win(system, fractal, generator);
    win.showMaximized();

    return app.exec();
}
