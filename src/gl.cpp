#include <gl.h>
#include <version.h>

#include <QKeyEvent>
#include <QFileDialog>

GLWidget::GLWidget(QWidget* parent)
    : QOpenGLWidget(parent)
{

}

void GLWidget::initializeGL()
{
    auto c = context();
    auto format = c->format();
    format.setSamples(4);
    c->setFormat(format);
    create();

    initializeLogger();
    view.initialize();

    auto f = QOpenGLContext::currentContext()->versionFunctions<opengl::Functions>();
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_MULTISAMPLE);

    emit finishInitialize();
}

void GLWidget::initializeLogger()
{
    logger = new QOpenGLDebugLogger();
    logger->initialize();
    connect(logger, &QOpenGLDebugLogger::messageLogged, this, &GLWidget::log);
    logger->startLogging();
}


void GLWidget::resizeGL(int width, int height)
{
    view.resize(width, height);
}

void GLWidget::paintGL()
{
    auto f = QOpenGLContext::currentContext()->versionFunctions<opengl::Functions>();

    emit preparePaint(view);

    f->glClear(GL_COLOR_BUFFER_BIT);

    emit finishPaint();
}

void GLWidget::saveImage()
{
    auto image = grabFramebuffer();
    auto file = QFileDialog::getSaveFileName(this, "Save Screenshot", "", "PNG (*.png);; JPG (*.jpg)");
    if (file.isEmpty())
    {
        return;
    }

    if (!image.save(file))
    {
        image.save(file + ".png");
    }
}

void GLWidget::toggleAA()
{
    GLboolean enabled;

    auto f = QOpenGLContext::currentContext()->versionFunctions<opengl::Functions>();
    f->glGetBooleanv(GL_MULTISAMPLE, &enabled);

    if (enabled)
    {
        glDisable(GL_MULTISAMPLE);
    }
    else
    {
        glEnable(GL_MULTISAMPLE);
    }

    update();
}

void GLWidget::keyPressEvent(QKeyEvent* e)
{
    if (view.handleKey(e->key()))
    {
        update();
        return;
    }

    switch (e->key())
    {
    case Qt::Key_P:
        saveImage();
        break;
    case Qt::Key_M:
        toggleAA();
        break;
    default:
        QOpenGLWidget::keyPressEvent(e);
    }
}

void GLWidget::log(const QOpenGLDebugMessage& m)
{
    qDebug() << m.severity();
    qDebug() << m.source();
    qDebug() << m.type();
    qDebug() << m.message() << "\n";
}

void GLWidget::setViewCenter(Vertex center)
{
    qDebug() << "Center: " << center.x << ", " << center.y;
    view.lookAt(center);
}
