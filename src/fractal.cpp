#include <fractal.h>
#include <version.h>

#include <QOpenGLContext>


void Fractal::initialize()
{
    shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":vertex-shader");
    shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":fragment-shader");
    shaderProgram.link();
    shaderProgram.bind();

    lineObject.initialize();
    faceObject.initialize();

    shaderProgram.release();
}

void Fractal::update(Generator& generator)
{
    lineObject.update(generator.moveLines());
    faceObject.update(generator.moveFaces());
}

void Fractal::prepare(View& view)
{
    view.synchronize(shaderProgram);
}

void Fractal::paint()
{
    shaderProgram.bind();

    lineObject.paint(GL_LINE_STRIP);
    faceObject.paint(GL_TRIANGLE_FAN);

    shaderProgram.release();
}

Vertex Fractal::center() const
{
    return lineObject.center();
}
#include <iostream>
void Fractal::syncColors(const std::vector<Color>& colors)
{
    shaderProgram.bind();
    auto location = shaderProgram.uniformLocation("colors");
    std::cout << "location is: " << location << "\n";
    auto data = reinterpret_cast<const GLfloat*>(colors.data());
    shaderProgram.setUniformValueArray(location, data, colors.size(), 3);
}
